import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// const name = 'John Smith'
// const element = <h1>Hello, {name}</h1>; //JSX - Javascript XML

// const root = ReactDOM.createRoot(document.getElementById('root'));

// root.render(element);

//<></>


