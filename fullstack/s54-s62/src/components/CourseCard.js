import { useState } from 'react'; 
import { Card, Button } from "react-bootstrap";
import PropTypes from 'prop-types'

//ACTIVITY S54
export default function CourseCard({courseProp}) {

  // console.log(props);
  // console.log(typeof props);

  const {name, description, price} = courseProp;

  //Syntax
    // const [getter, setter] = usestate

  const [count, setCount] = useState(0);

  console.log(useState(0))

  function enroll(){
    setCount(count + 1);
    console.log('Enrollees: ' + count);
  }

  //const [seats, setSeats] = useState(30);


  return (
    <Card>
      <Card.Body>
          <Card.Title>{name}</Card.Title>
          	<Card.Subtitle>Description:</Card.Subtitle>
          		<Card.Text>{description}</Card.Text>
         	 	<Card.Subtitle>Price:</Card.Subtitle>
         			<Card.Text>{price}</Card.Text>
              <Card.Text>Enrollees: {count}</Card.Text>
            <Button variant="primary" onClick={enroll}>Enroll</Button>
      </Card.Body>
    </Card>
  );
}



/// export default function CourseCard({courseProp}) {

// const {name, description, price} = courseProp;

// //ACTIVITY S55
// const [count, setCount] = useState(0);
// const [seats, setSeats] = useState(30);
  
//     function enroll() {
//       if (seats > 0) {
//         setCount(count + 1);
//         setSeats(seats - 1);
//       } else {
//         alert("No more seats.");
//       }
//     } 

//   return (
//     <Card>
//       <Card.Body>
//           <Card.Title>{name}</Card.Title>
//              <Card.Subtitle>Description:</Card.Subtitle>
//                <Card.Text>{description}</Card.Text>
//              <Card.Subtitle>Price:</Card.Subtitle>
//                <Card.Text>{price}</Card.Text>
//                <Card.Text>Enrollees: {count}</Card.Text>
//             <Button variant="primary" onClick={enroll}>Enroll</Button>
//       </Card.Body>
//     </Card>
//   );
// }

// // Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
   course: PropTypes.shape({
     name: PropTypes.string.isRequired,
     description: PropTypes.string.isRequired,
     price: PropTypes.number.isRequired
   })
 }