//[MongoDB Aggregation]
/*
	Going to join data from multiple documents
	Aggregation gives us access to manipulate, filter, and compute for results providing us with information to make necessary development decisions without having to create a Frontend application
*/

//db - market_db
//collection - fruits

//In mongosh, you can create a db. You can simply use - use <db>
//In mongosh, you can create a collection. You can simply use - db.createCollection(<collectionName>)

	//Create documents to use for the discussion


db.fruits.insertMany([
	{
		name : "Apple",
		color : "Red",
		stock : 20,
		price: 40,
		supplier_id : 1,
		onSale : true,
		origin: [ "Philippines", "US" ]
	},

	{
		name : "Banana",
		color : "Yellow",
		stock : 15,
		price: 20,
		supplier_id : 2,
		onSale : true,
		origin: [ "Philippines", "Ecuador" ]
	},

	{
		name : "Kiwi",
		color : "Green",
		stock : 25,
		price: 50,
		supplier_id : 1,
		onSale : true,
		origin: [ "US", "China" ]
	},

	{
		name : "Mango",
		color : "Yellow",
		stock : 10,
		price: 120,
		supplier_id : 2,
		onSale : false,
		origin: [ "Philippines", "India" ]
	}
]);

// Using the aggregate method
/*

	Syntax
		db.collectionName.aggregate([
			{ $match: { fieldA: valueA } },
			{ $group: { _id: "$fieldB" }, { result: { operation } } }
		])

	The "$match" is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process.
	- Syntax
		- { $match: { field: value } }
	
	- The "$group" is used to group elements together and field-value pairs using the data from the grouped elements
	- Syntax
		- { $group: { _id: "value", fieldResult: "valueResult" } }

*/

	//Using both "$match" and "$group" along with aggregation will find for fruitts that are on sale and will group the total amount of stocks for all suppliers found. 

db.fruits.aggregate([

		{ $match : { onSale : true } },
		{ $group : { _id : "$supplier_id", total : { $sum : "$stock" } } }
	]);
	//_id 1, total 45
	//_id 2, total 15


// Field projection with aggregation
/*
	The "$project" can be used when aggregating data to include/exclude fields from the returned results
	Syntax
		- { $project : { field: 1/0 } }
*/
db.fruits.aggregate([
		{ $match : { onSale : true } },
		{ $group : { _id : "$supplier_id", total : { $sum : "$stock" } } },
		{ $project : { _id : 0 } }
	]);

//total 15
//total 45

// Sorting aggregated results
/*
	The "$sort" can be used to change the order of aggregated results
	Providing a value of -1 will sort the aggregated results in a reverse order
	Syntax
		{ $sort { field: 1/-1 } }
	//1 (ascending order)
	//-1(descending order)
*/

db.fruits.aggregate([
		{ $match : { onSale : true } },
		{ $group : { _id : "$supplier_id", total : { $sum : "$stock" } } },
		{ $sort : { total : -1 } }
	]);
	//total 45 (descending from large to small)
	//total 15

	db.fruits.aggregate([
		{ $match : { onSale : true } },
		{ $group : { _id : "$supplier_id", total : { $sum : "$stock" } } },
		{ $sort : { total : 1 } }
	]);
	//total 15 (ascending from small to large)
	//total 45

// Aggregating values/results based on array fields
/*
	The "$unwind" deconstructs an array field from a collection/field with an array value to output a result for each element.
	The syntax below will return results creating separate documents for each of the countries provided per the "origin" field
	Syntax
		{ $unwind: field }
*/
db.fruits.aggregate([
	{ $unwind : "$origin" }
]);
//We have now 8 separate results based on array elements

//$sum calculates and gives the collective sum of number values
//The 1 passed in $sum is used to increment the count for each groub by 1

db.fruits.aggregate([
	{ $unwind : "$origin" },
	{ $group : { _id : "$origin" , kinds : { $sum : 1 } } }
]);

//MA1: find how many fruits are yellow (5 mins)
	//use $match and $count

db.fruits.aggregate([
		{ $match: {color:"Yellow"}},
		{ $count: "yellowfruits" }
	]);

db.fruits.aggregate([
		{ $match: {color: {$regex:'yellow',$options:'i'}}},
		{ $count: "yellowfruits" }
	]);
/*- Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stocks for all suppliers found.

	- The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
	- The "$sum" operator will total the values of all "stock" fields in the returned documents that are found using the "$match" criteria.*/
// [Section] Guidelines on Schema Design
/*
	- Schema design/data modelling is an important feature when creating databases.
	- MongoDB documents can be categorized into normalized/referenced and de-normalized/embedded data.
	- Normalized data refers to a data structure where documents are referred to each other using their ids for related pieces of information.
	- De-normalized data/embedded data refers to a data structure where related pieces of information is added to a document as an embedded object.
	- Both data structures are common practice but each of them have their pros and their cons.
	- Normalized data makes it easier to read information because separate documents can be retrieved but in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documents at the same time.
	- This approach is recommended for data structures where pieces of information are commonly operated on/changed.
	- De-normalized data/embedded data makes it easier to query documents and has a faster performance because only one query needs to be done in order to retrieve documents. However, if the data structure becomes too complex it makes it more difficult to manipulate and access information.
	- This approach is recommended for data structures where pieces of information are commonly retrieved and rarely operated on/changed.
*/

var owner = ObjectId("6474429f7c5a34544a4eaa68")


// One-To-One Relationship
db.owners.insertOne({
	_id : owner //automatically added once document is created
	name : "John Smith",
	contact : "09123456789"
});

db.suppliers.insertOne({
	name : "ABC Fruits",
	contact : "1234567890",
	owner_id : owner 
});

// One-To-Few Relationship
db.suppliers.insertOne({
	name : "DEF Fruits",
	contact : "1234567890",
	addresses : [
		{ street : "123 San Jose St.", city : "Manila" },
		{ street : "367 Gil Puyat", city : "Makati" }
	]
});

var supplier = ObjectId("647448567c5a34544a4eaa6b")
var branch1 = ObjectId("647449107c5a34544a4eaa6c")
var branch2 = ObjectId("647449b37c5a34544a4eaa6d")

// One-To-Many Relationship
db.suppliers.insertOne({
	name : "GHI Fruits",
	contact : "1234567890",
	branches : [
		{ branch_id : ObjectId("647449107c5a34544a4eaa6c") },
		{ branch_id : ObjectId("647449b37c5a34544a4eaa6d") }
	]
});

db.branches.insertOne({
	name : "BF Homes",
	address : "123 Arcardio Santos St.",
	city : "Parañaque",
	supplier_id : ObjectId("647448567c5a34544a4eaa6b")
});

db.branches.insertOne({
	name : "Rizal",
	address : "123 San Jose St.",
	city : "Manila",
	supplier_id : ObjectId("647448567c5a34544a4eaa6b")
});

db.suppliers.updateOne(
	{ name : "GHI Fruits" },
	{ $set : { branches : [
		{ branch_id : ObjectId("647449107c5a34544a4eaa6c") },
		{ branch_id : ObjectId("647449b37c5a34544a4eaa6d") }
	] }}
);

db.suppliers.updateOne (
    {name: "GHI Fruits"},
    {
        $set: {
            branches: [
                { branch_id: ObjectId("647448e1610cbf49632ff8be") },
                { branch_id: ObjectId("6474498c610cbf49632ff8bf") }
            ]
        }
    }
)
