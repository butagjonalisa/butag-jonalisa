console.log("Hello B297!");

//Functions
	//Parameters and Arguments

	/*function printName(){

		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname);
	}

	printName();*/

	function printName(name){

		console.log("Hi, " + name);
	}

	printName("Jo");

	let sampleVariable = "Cardo";

	printName(sampleVariable);


	function checkDivisibilityBy8(num){

		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);
	checkDivisibilityBy8(9678);

	/*
		Mini-Activity
		check the divisibility of a number by hav
	*/

	function checkDivisibilityBy4(num){

		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);

	//Functions as arguments
	//Function parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	//Adding and removing the parentheses "()" impacts the output of JS heavily
	//when a function is used with parentheses "()", it denotes invoking/calling a function
	//A function used without a parenthesis is normally associated with using the function as an argument to another function

	invokeFunction(argumentFunction);

	//Using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	};

	createFullName('Juan','Dela','Cruz');
	createFullName('Cruz','Dela','Juan');
	createFullName('Juan','Dela');
	createFullName('Juan','','Cruz');
	createFullName('Juan','Dela','Cruz','III');

	//Using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	//mini-activity

	createFullName(firstName,middleName,lastName);

	function printFriends(friend1, friend2, friend3){
		console.log("My three friends are: " + friend1 + "," + friend2 + "," + friend3 + ".");
	};

	printFriends("Jen","Flor","Shel");

	//Return Statement

	function returnFullName(firstName,middleName,lastName){

		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This will be printed!");

	}

	let completeName1 = returnFullName("Monkey","D","Luffy");
	let completeName2 = returnFullName("Cardo","Tanggol","Dalisay");

	console.log(completeName1 + " is my bestfriend!");
	console.log(completeName2 + " is my friend!");

	/*
		Mini Activity
		1. Create a function that will calculate an area of a square 
		2. Create a function that will add 3 numbers
		3. 
	*/

	function calculateArea(){

		let area = 0;
		let side = 3;
		return side * side;
		
	}

	console.log("Square: " + calculateArea("square"));

	function addNumbers(){
		
		let num1 = 7;
		let num2 = 15;
		let num3 = 21;
		return num1 + num2 + num3;
	}

	console.log("The sum of 3 numbers is " + addNumbers());

	function isequalOneHundred(num){
		return num === 100;

	}

	let booleanvalue = isequalOneHundred(77);
	console.log("Is this equal to One Hundred?");
	console.log(booleanvalue);
	
